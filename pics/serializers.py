from rest_framework import serializers
from pics.models import Pic


class PicPublicSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pic
        fields = ( 'title', 'url',)

class PicPrivateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pic
        fields = ( 'title', 'url','private',)



class PicDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pic
        fields = ('id', 'url', 'title', 'description','longitude','latitude','create_date','change_date','private',)

