from django.db import models

# Create your models here.


class Pic (models.Model):

    url = models.CharField(max_length=256)
    title = models.CharField(max_length=256)
    description = models.CharField(max_length=256)
    longitude = models.FloatField(default=0.0)
    latitude = models.FloatField(default=0.0)
    create_date = models.DateTimeField()
    change_date = models.DateTimeField()
    private = models.BooleanField(default=False)


def __unicode__(self):
    return self.title + ' ' + self.change_date
