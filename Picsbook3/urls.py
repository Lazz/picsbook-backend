from django.conf.urls import patterns, include, url
from rest_framework.routers import DefaultRouter
from pics import api
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    #Browsable
    url(r'^pics/public/', 'pics.views.pics_publiclist'),
    url(r'^pics/private/', 'pics.views.pics_privatelist'),

    # API
    url(r'^api/pics/public/$', api.PicPublicList.as_view()),
    url(r'^api/pics/private/$', api.PicPrivateList.as_view()),
    url(r'^api/pics/(?P<pk>[0-9]+)/$', api.PicDetail.as_view()),

)
