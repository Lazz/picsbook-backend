# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=256)),
                ('title', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=256)),
                ('longitude', models.FloatField(default=0.0)),
                ('latitude', models.FloatField(default=0.0)),
                ('create_date', models.DateTimeField()),
                ('change_date', models.DateTimeField()),
                ('private', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
