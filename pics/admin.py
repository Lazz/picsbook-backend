from django.contrib import admin
from pics.models import Pic

# Register your models here.

class PicAdmin(admin.ModelAdmin):

    fieldsets = (
            ('Titulo', {
                'fields' : ('title',),
                'classes' : ('wide',)
            }),

            ('URL', {
                'fields' : ( 'url',),
                'classes' : ('wide',)
            }),
            ('Coordenadas', {
              'fields': (('longitude', 'latitude'),),
              'classes' : ('wide', ),
            }),
            ('Fechas',{
              'fields': (('create_date','change_date'),),
              'classes' : ('wide',),
            }),
            ('Descripcion', {
                'fields' : ('description',),
                'classes' : ('wide',)
            }),
            ('Pri', {
                'fields' : ('private',),
                'classes' : ('wide',)
            }),
    )

admin.site.register(Pic, PicAdmin)
