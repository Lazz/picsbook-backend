from django.shortcuts import render
from django.http import HttpResponse
from pics.models import Pic

def pics_publiclist(request):
    print "Acceden al listado de fotos"
    listado_pics = []
    for p in Pic.objects.all():
        if (p.private == False):
             listado_pics.append(p)

    context = { 'pics' : listado_pics }
    return render(request, 'publicList.html', context)

def pics_privatelist(request):
    print "Acceden al listado privado de fotos"
    listado_pics = Pic.objects.all()
    context = { 'pics' : listado_pics }
    return render(request, 'privateList.html', context)
