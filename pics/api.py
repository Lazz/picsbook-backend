from pics.models import Pic
from pics.serializers import PicPublicSerializer, PicPrivateSerializer, PicDetailSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class PicPublicList(APIView):
    """
    List all pics, or create a new pic.
    """
    def get(self, request, format=None):
        pics = []
        for p in Pic.objects.all():
            if (p.private == False):
                pics.append(p)

        serializer = PicPublicSerializer(pics, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PicPublicSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PicPrivateList(APIView):
    """
    List all pics, or create a new pic.
    """
    def get(self, request, format=None):
        pics = Pic.objects.all()
        serializer = PicPrivateSerializer(pics, many=True)
        return Response(serializer.data)

class PicDetail(APIView):
    """
    Retrieve, update or delete a pic instance.
    """
    def get_object(self, pk):
        try:
            return Pic.objects.get(pk=pk)
        except Pic.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        pic = self.get_object(pk)
        serializer = PicDetailSerializer(pic)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        pic = self.get_object(pk)
        serializer = PicDetailSerializer(pic, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        pic = self.get_object(pk)
        pic.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)